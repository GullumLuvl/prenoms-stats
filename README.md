Résumés des statistiques de prénoms donnés en France
====================================================

Calcule diverses statistiques à partir des données de l'INSEE (nombre de
prénoms par année).

Les prénoms sont normalisés (accents) et phonétisés préalablement.

#### Trie les prénoms par popularité sur une période donnée

    ./prenom_stat.py popu 1950:1959 > popu_1950-1959.tsv

#### Trie les prénoms par différentiel de popularité sur période donnée

Un lissage préalable est effectué sur les pourcentages annuel (largeur par défaut `-L 10`).

    # dans la période ]1954 - 5 ; 1954 + 5]
    ./prenom_stat.py pic --rayon 5 1954 > pic_1950-1959.tsv

#### Affiche la courbe de popularité (en pourcentage par année)

    ./prenom_stat.py plot --separe Marie Daenerys+Khalissi

#### Liste les prénoms similaires

* Orthographes alternatives mais prononciation similaire (défaut `--cle frson`):

        ./prenom_stat.py --genre F simi Marie Zoé
        # Mari   Marie,Mary,Mari,Marry
        # Zoe    Zoe,Zoey,Zoee,Zohe,Zhoe

* Prénoms avec le même "doublemetaphone":

        ./prenom_stat.py --genre F --cle doublemetaphone simi Marie Zoé

### Dépendances

- phonétisation: https://gitlab.com/gullumluvl/stupidflip
- [metaphone](https://github.com/oubiwann/metaphone) (`pip install metaphone`)
