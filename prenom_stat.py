#!/usr/bin/env python3


from sys import stderr, exit, argv
import os.path as op
import argparse as ap
import pandas as pd
import numpy as np
import unicodedata
from stupidflip import StupidTranslit, FR_FIRSTNAMES_SCRIPT
from metaphone import doublemetaphone


frson = StupidTranslit.fromscript(FR_FIRSTNAMES_SCRIPT)


def normalise_ascii(prenom):
    return unicodedata.normalize('NFKD', prenom).encode('ASCII', 'ignore').decode('utf-8').capitalize()

def explicit_doublemetaphone(text):
    p1, p2 = doublemetaphone(text)
    return (p1, p2 or p1)

def metaphone1(text):
    return doublemetaphone(text)[0]

def metaphone2(text):
    p1, p2 = doublemetaphone(text)
    return p2 or p1


DATABASE_FILE = op.join(op.abspath(op.dirname(__file__)), 'insee-prenoms_2021.csv')


def charge_stats():
    # Noms de colonne originaux: sexe, preusuel, annais, nombre
    p = pd.read_csv(DATABASE_FILE, sep=';',
                    names=['sexe', 'preusuel', 'annee', 'nombre'],
                    header=0, na_values=['XXXX'],
                    dtype={'sexe': np.uint8})

    # p['annee'] = pd.to_datetime(p.annee, format='%Y')
    # Pas pratique: utiliser un PeriodIndex serait plus logique

    p['prenom'] = p.preusuel.str.normalize('NFKD').str.encode('ASCII', 'ignore').str.decode('utf-8').str.capitalize()
    p['preusuel'] = p.preusuel.str.normalize('NFKC').str.capitalize()
    # Nombre de naissances dans l'année
    p['total_annee_sexe'] = p.groupby(['sexe', 'annee']).nombre.transform('sum')
    p['pourcent'] = p.nombre / p.total_annee_sexe * 100

    # p.annee.isna().sum()  # 37924 année non renseignée.
    return p


def ajoute_frson(p):
    print('Phonemisation...', file=stderr)
    prenoms = p.preusuel.replace('_prenoms_rares', np.NaN).dropna().unique()  # 32961
    normed_prenoms = frson.prepare_series(pd.Series(prenoms, index=prenoms, name='frson'))
    prenom_to_frson = normed_prenoms.apply(frson.encode)
    # Selectionne l'orthographe la plus fréquente
    prenom_total = p.groupby('preusuel', sort=False).nombre.sum()
    frson_to_prenom = pd.concat((prenom_to_frson, prenom_total), join='outer', axis=1, sort=False).groupby('frson', sort=False)['nombre'].idxmax().rename('forme_ppale')

    return p.join(prenom_to_frson, on='preusuel').join(frson_to_prenom, on='frson'), frson_to_prenom


def ajoute_doublemetaphone(p):
    prenoms = p.preusuel.replace('_prenoms_rares', np.NaN).dropna().unique()  # 32961

    phones = pd.DataFrame.from_records(pd.Series(prenoms).apply(doublemetaphone), columns=['phone1', 'phone2'], index=prenoms)
    phones.phone2.where(phones.phone2!='', phones.phone1, inplace=True)

    ph = pd.merge(p, phones, 'outer', left_on='prenom', right_index=True)

    #np.unique(ph.phone1).shape  # 5601
    #np.unique(ph.phone2).shape  # 5405

    # Visualise les prénoms regroupés sous le même "metaphone"
    #phone1_to_prenom = phones.reset_index().groupby('phone1')['index'].apply(np.unique).apply(list)#.to_dict()
    doublephone_to_prenom = phones.reset_index().groupby(['phone1', 'phone2'])['index'].unique().apply(','.join).rename('prenoms')#.to_dict()

    return ph, doublephone_to_prenom


TOUTES_ANNEES = pd.RangeIndex(1900, 2022, dtype=int)


def smooth(sub, window=5, annee_index=TOUTES_ANNEES):
    return sub.groupby('annee')[['nombre', 'pourcent']].sum().reindex(annee_index).fillna(0).rolling(window, min_periods=1, center=True).mean()
    # Idéalement, la moyenne glissante devrait être pondérée par le nombre total par année.

def smooth_all(p, cle='frson', window=5, annee_index=TOUTES_ANNEES):
    smoothed = p.groupby(cle).apply(smooth, window, annee_index)
    smoothed.index.rename('annee', level=-1, inplace=True)
    return smoothed.reset_index(level=-1)


def classement_popularite(ph, slices=[(1900, 2021)], cle='frson'):
    #annee_index = pd.RangeIndex(start, end+1, dtype=int)
    start, end = slices[0]
    selection = (ph.annee >= start) & (ph.annee <= end)
    for start, end in slices[1:]:
        selection |= (ph.annee >= start) & (ph.annee <= end)

    if cle == 'prenom':
        return ph[selection].groupby(cle)['pourcent'].mean().sort_values(ascending=False)

    colonnes = ['pourcent', 'forme_ppale']
    aggfuncs = {'pourcent': 'mean', 'forme_ppale': 'first'}
    return ph[selection].groupby(cle)[colonnes].agg(aggfuncs).sort_values('pourcent', ascending=False)

# Prénoms féminins les plus populaires entre 1920 et 1940:
#popu_20_40 = classement_popularite(ph.query('sexe==2'), slices=[(1920, 1940)])
#
#popu_00_20 = classement_popularite(ph.query('sexe==2'), slices=[(1900, 1920)])

# Pic de popularité ?

#from scipy.signal import find_peaks, find_peaks_cwt

# Prénoms dont le pic de popularité se trouve dans les années 30 ?

def classement_pic(ph, annee, rayon=5, extension=20, lissage=10, cle='frson'):
    debut, fin = annee - rayon - extension, annee + rayon + extension
    bord_inf, bord_sup = debut - lissage/2, fin + lissage/2
    selected = ph[(ph.annee > bord_inf) & (ph.annee <= bord_sup)]
    annee_index = pd.RangeIndex(debut+1, fin+1, dtype=int)
    print('Intervalle annees: [%g; [%g; %g]; %g]' % (debut+1, annee-rayon+1, annee+rayon, fin), file=stderr)
    # Slow step:
    print('Lissage en cours...', file=stderr)
    smoothed = smooth_all(selected, cle, lissage, annee_index)
    bins = np.array([debut,
                     annee - rayon,
                     annee + rayon,
                     fin])
    w1, w2, w3 = (max(1900, annee - rayon) - max(1900, debut),
                  min(2021, annee + rayon) - max(1900, annee - rayon),
                  min(2021, fin) - min(2021, annee + rayon))
    print('#annees par tranche: %g %g %g' % (w1, w2, w3), file=stderr)
    smoothed['fenetre'] = np.digitize(smoothed.annee.values, bins=bins, right=True)
    bin_sums = smoothed.pivot_table('pourcent', index=cle, columns='fenetre', aggfunc='sum')
    delta = bin_sums[2]/w2 - (bin_sums[1] + bin_sums[3])/(w1+w3)
    delta = delta.rename('delta_pourcent').sort_values(ascending=False)
    if cle == 'prenom':
        return delta
    else:
        return pd.concat((delta, selected.groupby(cle, sort=False)['forme_ppale'].first()), axis=1)


def fusionne_prenoms(p, *alternatives, cle='prenom'):
    """Additionne les comptages de plusieurs prénoms (remplace les lignes correspondantes)"""
    aggfuncs = {'nombre': 'sum', 'pourcent': 'sum',
                'preusuel': 'first', 'prenom': 'first', 'total_annee_sexe': 'first'}
    if 'phone1' in p:
        aggfuncs.update(phone1='first', phone2='first')
    if 'frson' in p:
        aggfuncs.update(frson='first', forme_ppale='first')
    fusions = []
    tous_noms = []
    for noms_alternatifs in alternatives:
        tous_noms.extend(noms_alternatifs)
        selected = p.loc[p[cle].isin(noms_alternatifs)]
        fusions.append(selected.groupby(['sexe', 'annee']).agg(aggfuncs).reset_index())
        fusions[-1][cle] = '+'.join(sorted(noms_alternatifs))
    return pd.concat((p.loc[~p[cle].isin(noms_alternatifs)], *fusions),
                     axis=0, ignore_index=True, copy=False)

## Examples:
# phF = ph.query('sexe==2')
# 
# pic_1930 = classement_pic(phF, 1930)
# pic_1920 = classement_pic(phF, 1920)
# pic_1920_dm = classement_pic(phF, 1920, cle=['phone1', 'phone2'])
# pic_1920_dm = pd.concat((pic_1920_dm, doublephone_to_prenom), axis=1, join='inner')
# pic_1920_dm.head(50)


NORMALISATION = {
        ('prenom',): normalise_ascii,
        ('frson',): frson,
        ('doublemetaphone',): explicit_doublemetaphone,
        ('phone1', 'phone2'): explicit_doublemetaphone,
        ('phone1',): metaphone1,
        ('phone2',): metaphone2}


if __name__ == '__main__':
    parser = ap.ArgumentParser(description=__doc__)
    parser.add_argument('-g', '--genre', choices=['M', 'F'], help='[%(default)s]')
    parser.add_argument('-c', '--cle', default='frson',
                        help='colonne utilisée (frson/prenom/preusuel/doublemetaphone/phone1/phone2), ou une liste de colonnes [%(default)s]')
    parser.add_argument('-L', '--lissage', type=int, default=10,
                        help='largeur pour la moyenne glissante des pourcentages annuels [%(default)d]')

    subparsers = parser.add_subparsers(title='commandes', dest='command')

    p_popu = subparsers.add_parser('popu')
    p_popu.add_argument('slices', nargs='+')

    p_pic = subparsers.add_parser('pic')
    p_pic.add_argument('annee', type=int)
    p_pic.add_argument('-r', '--rayon', type=int, default=5,
                       help='rayon de la zone de focus du pic (en années) [%(default)s]')
    p_pic.add_argument('-e', '--extension', type=int, default=20,
                       help="largeur de la zone à considérer de part et d'autre du pic [%(default)s]")

    p_plot = subparsers.add_parser('plot')
    p_plot.add_argument('noms', nargs='+', help='prénoms ou (double) metaphones')
    p_plot.add_argument('-s', '--separe', action='store_true', dest='subplots')
    p_plot.add_argument('-b', '--brut', action='store_true', help="Ne convertit pas les arguments donnés pour 'clé' vers le format attendu [par défaut, applique la normalisation prévue (frson ou doublemetaphone)]") 

    p_simi = subparsers.add_parser('simi', help='Renvoie prénoms similaires')
    p_simi.add_argument('prenoms', nargs='+')

    args = parser.parse_args()

    p = charge_stats()
    cle = tuple(args.cle.split(','))
    if len(cle) == 1:
        cle_frame = cle[0]
    else:
        cle_frame = list(cle)

    try:
        cle_frame.remove('doublemetaphone')
        cle_frame += ['phone1', 'phone2']
    except ValueError:
        pass
    except AttributeError:
        if cle_frame == 'doublemetaphone':
            cle_frame = ['phone1', 'phone2']

    if args.genre == 'F':
        p = p.query('sexe==2')
    elif args.genre == 'M':
        p = p.query('sexe==1')

    if set(cle).intersection(('doublemetaphone', 'phone1', 'phone2')):
        p, doublephone_to_prenom = ajoute_doublemetaphone(p)
    if 'frson' in cle:
        p, frson_to_prenom = ajoute_frson(p)

    if args.command == 'popu':
        slices = [tuple(int(x) for x in s.split(':')) for s in args.slices]
        result = classement_popularite(p, slices, cle=cle_frame)
        print(result.to_csv(sep='\t', float_format='%g'))

    elif args.command == 'pic':
        result = classement_pic(p, args.annee, args.rayon, args.extension, args.lissage, cle_frame)
        print(result.to_csv(sep='\t', float_format='%g'))

    elif args.command == 'plot':
        if not args.brut:
            try:
                normalise = NORMALISATION[cle]
            except KeyError:
                raise ValueError('Mauvaise clé %r' % cle)

            noms = []
            alternatives = []
            for nom in args.noms:
                if '+' in nom:
                    noms_alternatifs = sorted(set(normalise(n.capitalize()) for n in nom.split('+')))
                    alternatives.append(noms_alternatifs)
                    nom = '+'.join(noms_alternatifs)
                else:
                    nom = normalise(nom.capitalize())
                noms.append(nom)
            print('noms:', noms)
            if alternatives:
                p = fusionne_prenoms(p, *alternatives, cle=cle_frame)
                #print('Fusion:', file=stderr)
                #print(p[p[cle].isin(noms)].tail(), file=stderr)
            #selected = p.loc[p[cle_frame].isin(noms)]
        elif cle == ('doublemetaphone',) or cle == ('phone1', 'phone2'):
            noms = [tuple(x.upper() for x in s.split(',')) for s in args.noms]
        else:
            noms = [nom.capitalize() for nom in args.noms]
        #selected = p.loc[p[cle_frame].isin(noms)]
        selected = p.set_index(cle_frame).loc[noms].reset_index()

        if selected.shape[0] == 0:
            print('Prénoms %s = "%s" non trouvés' % (' '.join(args.noms), ' '.join(noms)),
                  file=stderr)
            exit(1)
        smoothed = smooth_all(selected, cle_frame, args.lissage)
        import matplotlib as mpl
        mpl.use('TkAgg')
        import matplotlib.pyplot as plt
        data = smoothed.reset_index().set_index('annee').pivot(columns=cle_frame)['pourcent']
        axes = data.plot(legend=True, subplots=args.subplots, grid=True,
                         ylabel='%/an', kind=('area' if args.subplots else 'line'))
        if isinstance(axes, mpl.axes.Axes):
            axes = [axes]
        for ax in axes:
            ax.spines['top'].set_visible(False)
            ax.spines['right'].set_visible(False)
        ax.set_xlim(1900, 2021)
        plt.show(block=True)
        
    elif args.command == 'simi':
        if 'phone1' in p and args.genre:
            # Ajoute seulement les metaphones du genre donné.
            p, doublephone_to_prenom = ajoute_doublemetaphone(p.drop(columns=['phone1', 'phone2']))
        elif not 'phone1' in p:
            p, doublephone_to_prenom = ajoute_doublemetaphone(p)

        normalise = NORMALISATION[cle]

        if cle == ('doublemetaphone',) or cle == ('phone1', 'phone2'):
            for nom in args.prenoms:
                p1, p2 = normalise(nom)
                simi = doublephone_to_prenom.loc[(p1, p2)]
                print(nom, p1+','+p2, simi, sep='\t')
        else:
            for nom in args.prenoms:
                normed = normalise(nom.capitalize())
                simi = p.loc[p[cle_frame]==normed, ['prenom', 'nombre']].groupby('prenom', sort=False)['nombre'].sum().sort_values(ascending=False).index.tolist()
                print(normed, ','.join(simi), sep='\t')
        

# Cluster similar firstnames:
#  https://stats.stackexchange.com/questions/123060/clustering-a-long-list-of-strings-words-into-similarity-groups
#  https://towardsdatascience.com/python-tutorial-fuzzy-name-matching-algorithms-7a6f43322cc5

# Distance methods:
# - Levenshtein (edit distance: insertion, deletion, substitution)
#   - Damerau-Levenshtein (adds transposition of two adjacent characters)
# - by prononciation: double-metaphone
#
# Clustering algo
# - Affinity Propagation
# - Louvain clustering
# - Markov Cluster algorithm (MCL)
# - Restricted Neighborhood Search Clustering (RNSC)

